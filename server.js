var express = require("express");
var app = express();

app.use(express.static(__dirname + "/build"));

app.set("port", 5000);

app.listen(app.get("port"), function() {
    console.log("Server is running on port", app.get("port"));
});
