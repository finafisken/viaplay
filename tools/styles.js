var sass = require("node-sass");
var fs = require("fs-extra");

sass.render({
        file: "src/scss/main.scss",
        outFile: "public/css/styles.css",
        outputStyle: "compressed"
    },
    function(error, result) {
        if(!error){
            // No errors during the compilation, write this result on the disk
            fs.outputFile("public/css/styles.css", result.css, function(err){
                if(!err){
                    // File written on disk
                    console.log("Compiled SCSS successfully");
                } else {
                    console.log(err);
                }
            });
        } else {
            console.log(error);
        }
    }
);
