var fs = require("fs-extra");
var rimrafSync = require("rimraf").sync;
var webpack = require("webpack");
var config = require("../webpack.config.prod");
var recursive = require("recursive-readdir");

recursive("build", (err, fileNames) => {

    // Clean build folder
    rimrafSync("build/*");

    // Webpack build
    build();

    // Copy content from public folder
    fs.copySync("public", "build");
});

function build() {
    console.log("Creating an optimized build...");
    webpack(config).run((err, stats) => {
        if (err) {
          console.error("Failed to compile.", [err]);
          process.exit(1);
        }

        if (stats.compilation.errors.length) {
          console.error("Failed to compile.", stats.compilation.errors);
          process.exit(1);
        }
        console.log("Compiled successfully.");
    });
}
