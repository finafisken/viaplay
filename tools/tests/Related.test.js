import React from "react";
import TestUtils from "react-addons-test-utils";
import Related from "../../src/js/components/Related";

describe("Related component", () => {
    let renderedComp = null,
        relatedClickHandler = jest.fn();
    const testData = require("./testData/starwars.json");

    it("Renders Related component", () => {
        renderedComp = TestUtils.renderIntoDocument(
            <Related data={testData.related} clickHandler={relatedClickHandler}/>
        );
    });

    it("Calls clickHandler when a movie is clicked", () => {
        const relatedMovies = TestUtils.scryRenderedDOMComponentsWithClass(renderedComp, "related-movie");
        TestUtils.Simulate.click(relatedMovies[0]);
        expect(relatedClickHandler).toHaveBeenCalled();
    });
});
