import React from "react";
import TestUtils from "react-addons-test-utils";
import App from "../../src/js/components/App";

describe("App component", () => {
    let renderedApp = null;
    const testData = require("./testData/starwars.json");

    it("Renders App component", () => {
        renderedApp = TestUtils.renderIntoDocument(
            <App />
        );
    });
    it("Can update with new data", () => {
        renderedApp.update(testData);

        expect(renderedApp.state.movie.title).toBe("Star Wars: The Force Awakens");
        expect(renderedApp.state.related.length).toBeGreaterThan(0);
    });
    it("Displays new data", () => {
        const title = TestUtils.findRenderedDOMComponentWithClass(renderedApp, "movie-title");
        const relatedMovies = TestUtils.scryRenderedDOMComponentsWithClass(renderedApp, "related-movie");

        expect(title.textContent).toBe("Star Wars: The Force Awakens");
        expect(relatedMovies.length).toBeGreaterThan(0);
    });
});
