# Viaplay work sample

## About

Viaplay work sample made by Markus Warne. The application displays a movie asset from the Viaplay Content API.
Users can navigate between movies through selecting another movie in the "Similar movies" grid.

### Tech

- Development environment set up in [Node.js](https://nodejs.org/en/)
- The UI is made with [React](https://facebook.github.io/react/)
- Most of the asset bundling is made with [Webpack](https://webpack.github.io/)
- SCSS styles compiled with [Node-sass](https://github.com/sass/node-sass)
- Testing is done with [Jest](https://facebook.github.io/jest/)

The reason I chose React is that the modularized component aspect really appeals to me. Webpack and Jest were chosen since they play nicely with the react eco-system. Node was chosen due to the assignment requirement but this would also be my personal choice since I am familiar with the setup.

## Setup

```sh
git clone https://finafisken@bitbucket.org/finafisken/viaplay.git
cd viaplay
npm install
npm start
```

Open http://localhost:5000 in your browser.

Tested on Node v7.1.0 and npm 3.10.9

### Commands

#### `npm start`
Runs `build` and then hosts the build folder on http://localhost:5000

#### `npm run dev`
Launches webpack-dev-server on http://localhost:3333. This is the development version of the application

#### `npm test`
Runs Jest which will go through the tests found in tools/test
#### `npm run styles`
Compiles main.scss into styles.css through node-sass

#### `npm run build`
Runs `test`, `styles` and then bundles assets with Webpack before outputting a build folder.

## Not included / Future improvements
These things were not included due to time constraints

 - Watch scss folder and compile
 - Better test cases, missing data etc
 - Include linting in process, don't rely on editor
 - Router / deeplinking to assets
 - Better error handling, handle missing data etc
