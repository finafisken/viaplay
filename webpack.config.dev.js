var webpack = require("webpack");

module.exports = {
    entry: "./src/js/main.js",
    output: {
        path: "./build/",
        filename: "js/index.js"
    },
    devServer: {
        inline: true,
        port: 3333,
        contentBase: "./public/"
    },
    devtool: "source-map",
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel",
                query: {
                    presets: ["es2015", "react"]
                }
            }
        ]
    }
};
