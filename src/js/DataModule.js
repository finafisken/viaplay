export default class {

    getMovie(id) {
        return this.fetchData(id).then(this.parseMovie.bind(this));
    }

    fetchData (id) {
        if(!id) {
            return;
        }
        const url = "https://content.viaplay.se/pc-se/film/"+id;

        return new Promise(function(resolve, reject) {
            let xhr = new XMLHttpRequest();
            xhr.open("GET", url);
            xhr.onload = function () {
                if (xhr.status === 200) {
                    resolve(JSON.parse(xhr.response));
                } else {
                    reject(xhr.statusText);
                }
            };
            xhr.onerror = function () {
                reject("Failed to send XHR");
            };
            xhr.send();
        });
    }

    parseMovie (data) {
        if(!data) {
            return;
        }
        try {
            // @TODO should check for product type and handle missing data
            var movieData = data._embedded["viaplay:blocks"][0]._embedded["viaplay:product"],
                relatedArray = data._embedded["viaplay:blocks"][1]._embedded["viaplay:products"];
            return {
                movie: this.parseMovieMeta(movieData),
                related: relatedArray.map(this.parseRelatedMeta)
            };
        } catch (e) {
            console.warn(e);
        }
    }

    parseMovieMeta (data) {
        return {
            id: data.publicPath || null,
            title: data.content.title || null,
            duration: data.content.duration && data.content.duration.readable || null,
            synopsis: data.content.synopsis || null,
            imdb_rating: data.content.imdb && data.content.imdb.rating || null,
            image_url: data.content.images && data.content.images.landscape && data.content.images.landscape.url || null,
            actors: data.content.people && data.content.people.actors || null
        };
    }

    parseRelatedMeta (data) {
        return {
            title: data.content.title || null,
            image_url: data.content.images && data.content.images.boxart && data.content.images.boxart.url || null,
            id: data.publicPath || null
        };
    }
}
