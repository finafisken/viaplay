import React from "react";
import Header from "./Header";
import Movie from "./Movie";
import Related from "./Related";
import DataModule from "../DataModule";

let dataModule = new DataModule();
const defaultMovieId = "star-wars-episode-vii-the-force-awakens-2015";

class App extends React.Component {
    constructor(){
        super();
        this.state = {
            movie: {},
            related: []
        };
        this.relatedClickHandler = this.relatedClickHandler.bind(this);
        this.update = this.update.bind(this);
    }
    update(movieData) {
        if (!movieData || !movieData.movie || !movieData.related) {
            this.setState({
                missingData: true
            });
        } else {
            this.setState({
                movie: movieData.movie,
                related: movieData.related,
                missingData: false
            });
        }
    }
    componentWillMount(){
        dataModule.getMovie(defaultMovieId)
            .then(this.update)
            .catch((e) => console.warn(e));
    }
    relatedClickHandler(movieId){
        dataModule.getMovie(movieId)
            .then(this.update)
            .catch((e) => console.warn(e));
    }
    render (){
        let noContent = <h2 className="no-content">Something went wrong...</h2>,
            content = <div>
                    <Movie data={this.state.movie} />
                    <Related data={this.state.related} clickHandler={this.relatedClickHandler} />
                </div>;
        return (
            <div className="app-wrap">
                <Header />
                { this.state.missingData ? noContent : content }
            </div>
        );
    }
}
export default App;
