import React from "react";
import Hero from "./Hero";
import MetaContainer from "./MetaContainer";

class Movie extends React.Component {
    render (){
        return (
            <div className="movie-container">
                <Hero
                    title={this.props.data.title}
                    imageUrl={this.props.data.image_url}
                 />
                <MetaContainer
                    imdbRating={this.props.data.imdb_rating}
                    duration={this.props.data.duration}
                    synopsis={this.props.data.synopsis}
                    actors={this.props.data.actors}
                />
            </div>
        );
    }
}
export default Movie;
