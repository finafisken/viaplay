import React from "react";

const Hero = (props) => {
    let heroBg = {
        backgroundImage: "url(" + props.imageUrl + ")"
    };
    return  (
        <div className="movie-hero" style={heroBg}>
            <div className="movie-fake-play">
                <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ" target="_blank">
                    PLAY ME!
                </a>
            </div>
            <h1 className="movie-title">{props.title}</h1>
        </div>
    );
};
export default Hero;
