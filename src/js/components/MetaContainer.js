import React from "react";

const MetaContainer = (props) => {
    let actors = props.actors && props.actors.length > 0 ? props.actors.join(", ") : null;
    return  (
        <div className="movie-meta-container">
            <span className="movie-imdb">IMDb: {props.imdbRating}</span>
            <span className="movie-duration">{props.duration}</span>
            <p className="movie-synopsis">{props.synopsis}</p>
            <p className="movie-actors">
                {actors ? <span>Starring</span> : null} {actors}
            </p>
        </div>
    );
};
export default MetaContainer;
