import React from "react";

const Header = () => {
    return  (
        <div className="header-wrap">
            <div className="header">
                <img src="img/logo.png" />
            </div>
        </div>
    );
};
export default Header;
