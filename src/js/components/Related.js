import React from "react";
import RelatedMovie from "./RelatedMovie";

class Related extends React.Component {
    render (){
        let related = this.props.data.map(movie => {
            return (
                <RelatedMovie
                    key={movie.id}
                    clickHandler={this.props.clickHandler.bind(this, movie.id)}
                    title={movie.title}
                    imageUrl={movie.image_url}
                />
            );
        });
        return (
            <div className="related-wrap">
                <h2>Similar movies</h2>
                <ul>
                    {related}
                </ul>
            </div>
        );
    }
}
export default Related;
