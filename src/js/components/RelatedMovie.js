import React from "react";

const RelatedMovie = (props) => {
    return  (
        <li className="related-movie" onClick={props.clickHandler}>
            <h3>{props.title}</h3>
            <div className="overlay"></div>
            <img src={props.imageUrl}></img>
        </li>
    );
};
export default RelatedMovie;
